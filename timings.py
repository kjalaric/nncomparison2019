'''
Code used for STAT448 Assignment 3 question 2: comparison of different neural network models.
Based on code provided for the course.

Brook O'Reilly
'''
import warnings
warnings.filterwarnings("ignore")

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json
from keras.models import Sequential
from keras.layers import Dense, Dropout, BatchNormalization, Activation
from keras.optimizers import *
from keras.utils.np_utils import to_categorical
import keras.datasets.fashion_mnist as mnist
import time

# available optimisers
opt_dict = {'nadam':Nadam, 'adam':Adam, 'sgd':SGD, 'rmsprop':RMSprop, 'adadelta':Adadelta, 'adagrad':Adagrad}

def getHyperparameters(learning_rates, batch_sizes, training_epochs, optimisers):
    configs = list()
    for a in learning_rates:
        for b in batch_sizes:
            for e in training_epochs:
                for o in optimisers:
                    configs.append([a, b, e, o])
    return configs

def plot_loss_accuracy(history):
    historydf = pd.DataFrame(history.history, index=history.epoch)
    historydf.plot(ylim=(0, max(1, historydf.values.max())))
    loss = history.history['loss'][-1]
    acc = history.history['accuracy'][-1]
    plt.title('{}\nLoss: {:.3f}, Accuracy: {:.3f}'.format(model.name_for_plot, loss, acc))
    # plt.savefig(model.filename_for_plot+".png")

def buildBasicMLP(unit_size, meta_string):
    model = Sequential()
    model.name_for_plot = "Basic MLP (unit size = {}) - {}".format(unit_size, meta_string)
    model.filename_for_plot = "basic_mlp_{}_{}".format(unit_size, meta_string)
    model.add(Dense(units=unit_size, activation='relu', input_shape=(image_vector_size,)))
    model.add(Dense(units=num_classes, activation='softmax'))
    return model

def buildMLPWithDropout(dropout, meta_string):
    model = Sequential()
    model.name_for_plot = "MLP with dropout (dropout = {}) - {}".format(dropout, meta_string)
    model.filename_for_plot = "dropout_mlp_{}_{}".format(dropout, meta_string)
    model.add(Dense(units=32, activation='sigmoid', input_shape=(image_vector_size,)))
    model.add(Dense(units=num_classes, activation='softmax'))
    return model

def buildDeepMLP(depth, meta_string):
    model = Sequential()
    assert depth > 1, "Depth for DeepMLP must be at least 2."
    model.name_for_plot = "Deep MLP (number of layers = {}) - {}".format(depth, meta_string)
    model.filename_for_plot = "deep_mlp_{}_{}".format(depth, meta_string)
    model.add(Dense(units=64, activation='sigmoid', input_shape=(image_vector_size,)))
    for _ in range(depth-1):
        model.add(Dense(units=64, activation='sigmoid'))
    model.add(Dense(units=num_classes, activation='softmax'))
    return model
  
def buildDeepMLPWithDropout(dropout, depth, meta_string):
    # this function only permits uniform dropout
    model = Sequential()
    assert depth > 1, "Depth for DeepMLP must be at least 2."
    model.name_for_plot = "Deep MLP with dropout (dropout = {}, number of layers = {}) - {}".format(dropout, depth, meta_string)
    model.filename_for_plot = "deep_mlp_{}_dropout_{}_{}".format(depth,dropout,meta_string)
    model.add(Dense(units=64, activation='sigmoid', input_shape=(image_vector_size,)))
    for _ in range(depth-1):
        model.add(Dropout(dropout))
        model.add(Dense(units=64, activation='sigmoid'))
    model.add(Dense(units=num_classes, activation='softmax'))
    return model

def buildModel(model_type, parameters, meta_string):
    if model_type == "basicMLP":
        return buildBasicMLP(parameters["unit_size"], meta_string)
    elif model_type == "MLPWithDropout":
        return buildMLPWithDropout(parameters["dropout"], meta_string)
    elif model_type == "deepMLP":
        return buildDeepMLP(parameters["depth"], meta_string)
    elif model_type == "deepMLPWithDropout":
        return buildDeepMLPWithDropout(parameters["dropout"], parameters["depth"], meta_string)


if __name__ == "__main__":
    # load training data
    (x_train, y_train), (x_test, y_test) = mnist.load_data()
    image_size = (x_train.shape[1], x_train.shape[2])
    
    # flatten images
    image_vector_size = image_size[0] * image_size[1] # 28 * 28
    x_train = x_train.reshape(x_train.shape[0], image_vector_size) /255.
    x_test = x_test.reshape(x_test.shape[0], image_vector_size) /255.
    
    # one-hot categorisation
    num_classes = 10
    y_train = to_categorical(y_train, num_classes)
    y_test = to_categorical(y_test, num_classes)

    # these architectures will be evaluated
    architectures = [
        ["basicMLP", {"unit_size": 32}],
        ["basicMLP", {"unit_size": 64}],
        ["MLPWithDropout", {"dropout": 0.1}],
        ["MLPWithDropout", {"dropout": 0.2}],
        ["MLPWithDropout", {"dropout": 0.3}],
        ["deepMLP", {"depth": 2}],
        ["deepMLP", {"depth": 3}],
        ["deepMLP", {"depth": 10}],
        ["deepMLPWithDropout", {"dropout": 0.1, "depth": 3}],
        ["deepMLPWithDropout", {"dropout": 0.2, "depth": 3}],
        ]

    for architecture in architectures:
        for config in getHyperparameters(learning_rates=[0.01],
                                         batch_sizes=[100],
                                         training_epochs=[5, 10, 20],
                                         optimisers=['adam']):
        
            # metadata for hyperparameter comparison
            meta_string = "a{}_b{}_e{}_{}".format(config[0], config[1], config[2], config[3])
        
            # construct model as defined in 'architectures'
            model = buildModel(architecture[0], architecture[1], meta_string)
        
            # compile and train model
            optimiser = opt_dict[config[3]](lr=config[0])
            model.compile(optimiser, loss='categorical_crossentropy', metrics=['accuracy'])
            
            start = time.time()
            history = model.fit(x_train, y_train, batch_size=config[1], epochs=config[2], verbose=False, validation_split=.1)
            print("{}: {}".format(model.filename_for_plot, time.time() - start))
            
            

