
import json
from comparison import *

if __name__ == "__main__":
    with open("nn_comparison.json", "r") as fi:
        results = json.load(fi)
    
    with open("results.txt", "w") as fo:
        for x in sorted(results["accuracy"].items(), key = lambda x : x[1], reverse=True):
            if "e20" in x[0]:
                fo.write("{}: {}\n".format(x[0], x[1]))
        
        fo.write("======\n\n")
        for x in sorted(results["accuracy"].items(), key = lambda x : x[1], reverse=True):
            if "e10" in x[0]:
                fo.write("{}: {}\n".format(x[0], x[1]))
        
        fo.write("======\n\n")
        for x in sorted(results["accuracy"].items(), key = lambda x : x[1], reverse=True):
            if "e5" in x[0]:
                fo.write("{}: {}\n".format(x[0], x[1]))
    
    
    
    # architectures = list(set([getArchitectureType(x) for x in results["accuracy"].keys()]))