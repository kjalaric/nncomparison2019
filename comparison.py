'''
Script to compare different model architectures and hyperparameters for STAT448 Assignment 3.

Brook O'Reilly
'''

import json
import re
import matplotlib.pyplot as plt

model_types = [
    "basic_mlp",
    "dropout_mlp",
    "deep_mlp",  # includes one with dropout
    ]


valid_optimisers = ["adam", "sgd", "rmsprop"]

default_a = "a0.01"
default_b = "b100"
default_e = "e5"
default_o = "adam"

exclude_architectures = ["deep_mlp_10"]

def getArchitectureType(name_string):
    return name_string[:name_string.index("_a")]

def getChanges(items):
    changeset = dict()
    for itemlist in items.values():
        overlap = [x for x in itemlist[0][0].split("_") if x in itemlist[1][0].split("_")]
        exclude = False
        for exclusion in exclude_architectures:
            if exclusion in itemlist[0][0]:
                exclude = True
        if exclude:
            continue
        for index, pair in enumerate(itemlist):
            diff = [x for x in pair[0].split("_") if x not in overlap][0]
            if diff not in valid_optimisers:  # optimisers are text-based
                diff = float(re.sub(r'[a-z]+', '', diff, re.I))
            try:
                changeset[getArchitectureType(pair[0])].append((diff, pair[1]))
            except KeyError:
                changeset[getArchitectureType(pair[0])] = [(diff, pair[1])]
        
    return changeset


if __name__ == "__main__":
    with open("nn_comparison.json", "r") as fi:
        results = json.load(fi)
    
    architectures = list(set([getArchitectureType(x) for x in results["accuracy"].keys()]))
    
    alpha_compare = {key: list() for key in architectures}
    batch_size_compare = {key: list() for key in architectures}
    epochs_compare = {key: list() for key in architectures}
    optimiser_compare = {key: list() for key in architectures}
    for k, v in results["accuracy"].items():
        if "{}_{}_{}".format(default_a, default_b, default_e) in k:
            optimiser_compare[getArchitectureType(k)].append([k, v])
        if "{}_{}".format(default_a, default_b) in k and default_o in k:
            epochs_compare[getArchitectureType(k)].append([k, v])   
        if default_a in k and "{}_{}".format(default_e, default_o) in k:
            batch_size_compare[getArchitectureType(k)].append([k,v])
        if "{}_{}_{}".format(default_b, default_e, default_o) in k:
            alpha_compare[getArchitectureType(k)].append([k,v])
            
    if (1):
        av_y = [0, 0, 0]
        for k, v in getChanges(alpha_compare).items():
            x, y = zip(*sorted(v, key = lambda x : x[0]))
            plt.plot(x, y, linestyle='dotted', marker = 'o', label = k)
            for i, vv in enumerate(y):
                av_y[i] += vv/len(getChanges(alpha_compare).items())
        plt.plot([0.01, 0.05, 0.1], av_y, linestyle = "solid", label = "Average")
        plt.title("Impact of learning rate on accuracy for different network architectures")
        plt.xlabel("Learning rate")
        plt.ylabel("Model accuracy (%)")
        plt.grid()
        plt.legend()
        plt.show()
        
    if (1):
        av_y = [0, 0, 0]
        for k, v in getChanges(batch_size_compare).items():
            x, y = zip(*sorted(v, key = lambda x : x[0]))
            plt.plot(x, y, linestyle='dotted', marker = 'o', label = k)
            for i, vv in enumerate(y):
                av_y[i] += vv/len(getChanges(alpha_compare).items())
        plt.plot([100, 500, 1000], av_y, linestyle = "solid", label = "Average")
        plt.title("Impact of batch size on accuracy for different network architectures")
        plt.xlabel("Batch size")
        plt.ylabel("Model accuracy (%)")
        plt.grid()
        plt.legend()
        plt.show()
        
    if (1):
        for k, v in getChanges(optimiser_compare).items():
            x, y = zip(*v)
            plt.scatter(x, y, marker = 'o', label = k)
        plt.title("Impact of optimiser type on accuracy for different network architectures")
        plt.xlabel("Optimizer")
        plt.ylabel("Model accuracy (%)")
        plt.grid()
        plt.legend()
        plt.show()
    
    
    